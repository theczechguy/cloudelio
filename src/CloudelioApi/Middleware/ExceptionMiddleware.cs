﻿using CloudelioApi.Exceptions;
using System.Net;

namespace CloudelioApi.Middleware
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<ExceptionMiddleware> _logger;

        public ExceptionMiddleware(RequestDelegate next, ILogger<ExceptionMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (AppException appEx)
            {
                _logger.LogWarning(appEx, "Error occured during request processing");
                await HandleAppExceptionAsync(httpContext, appEx);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Fatal error");
                await HandleFatalExceptionAsync(httpContext);
            }
        }

        private Task HandleFatalExceptionAsync(HttpContext httpContext)
        {
            httpContext.Response.ContentType = "application/json";
            httpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

            return httpContext.Response.WriteAsync(new ErrorDetails()
            {
                Message = "Fatal error. Please contact administrator",
                StatusCode = httpContext.Response.StatusCode
            }.ToString());
        }

        private Task HandleAppExceptionAsync(HttpContext httpContext, AppException appException)
        {
            httpContext.Response.ContentType = "application/json";
            httpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;

            return httpContext.Response.WriteAsync(new ErrorDetails()
            {
                Message = appException.Message,
                StatusCode = httpContext.Response.StatusCode
            }.ToString());
        }
    }
}