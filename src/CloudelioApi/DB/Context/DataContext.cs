﻿using CloudelioApi.DB.Entities;
using CloudelioApi.Entities.Models;
using Microsoft.EntityFrameworkCore;

namespace CloudelioApi.Entities
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions options)
            : base(options)
        {
        }

        public DbSet<Organization> Organizations { get; set; }
        public DbSet<CloudGroup> CloudGroups { get; set; }
        public DbSet<Secret> Secrets { get; set; }
        public DbSet<SecretStorage> SecretStorages { get; set; }
        public DbSet<CloudItemProvider> CloudItemProviders { get; set; }
        public DbSet<CloudItemProviderType> CloudItemProviderTypes { get; set; }
        public DbSet<CloudItemProviderSecret> CloudItemProvidersSecrets { get; set; }
        public DbSet<CloudItem> CloudItems { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Organization>()
                .HasOne(o => o.SecretStorage)
                .WithMany()
                .HasForeignKey(o => o.SecretStorageId)
                .OnDelete(DeleteBehavior.NoAction)
                .IsRequired();

            modelBuilder.Entity<Secret>()
                .HasOne(s => s.SecretStorage)
                .WithMany()
                .HasForeignKey(s => s.SecretStorageId)
                .OnDelete(DeleteBehavior.NoAction)
                .IsRequired();

            modelBuilder.Entity<Secret>()
                .HasOne(s => s.Organization)
                .WithMany()
                .HasForeignKey(s => s.OrganizationId)
                .OnDelete(DeleteBehavior.NoAction)
                .IsRequired();


            modelBuilder.Entity<CloudItem>()
                .HasOne(c => c.CloudItemProvider)
                .WithMany()
                .HasForeignKey(c => c.CloudItemProviderId)
                .OnDelete(DeleteBehavior.NoAction)
                .IsRequired();

            base.OnModelCreating(modelBuilder);
            SeedCloudItemTypes(modelBuilder);
        }

        private void SeedCloudItemTypes(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CloudItemProviderType>().HasData(new CloudItemProviderType
            {
                ItemTypeDescription = "ResourceGroup in Azure subscription",
                ItemTypeName = "Resource Group",
                ItemTypeId = Guid.NewGuid(),
                RequiredSecretPurposes = "TenantId,ClientId,ClientSecret"
            });
            //modelBuilder.Entity<CloudItemType>().HasData(new CloudItemType
            //{
            //    ItemTypeDescription = "New project in Google Cloud",
            //    ItemTypeName = "Google Cloud Project",
            //    ItemTypeId = Guid.NewGuid()
            //});
            //modelBuilder.Entity<CloudItemType>().HasData(new CloudItemType
            //{
            //    ItemTypeDescription = "New account in Amazon AWS",
            //    ItemTypeName = "AWS Account",
            //    ItemTypeId = Guid.NewGuid()
            //});
        }
    }
}