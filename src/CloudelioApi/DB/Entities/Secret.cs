﻿using CloudelioApi.Entities.Models;

namespace CloudelioApi.Entities
{
    public class Secret
    {
        public Guid SecretId { get; set; }
        public string SecretName { get; set; }
        public string KeyVaultSecretName { get; set; }
        public Guid OrganizationId { get; set; }
        public Organization Organization { get; set; }

        public Guid SecretStorageId { get; set; }
        public SecretStorage SecretStorage { get; set; }
        public string SecretPurpose { get; set; }
    }
}