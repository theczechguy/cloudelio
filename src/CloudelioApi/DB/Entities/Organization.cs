﻿using System.ComponentModel.DataAnnotations;

namespace CloudelioApi.Entities.Models
{
    public class Organization
    {
        [Key]
        public Guid OrganizationId { get; set; }

        [Required(ErrorMessage = "Organization name is required")]
        public string OrganizationName { get; set; }

        [Required(ErrorMessage = "Organization owner is required")]
        public string OrganizationOwner { get; set; }

        public DateTime OrganizationCreated { get; set; }

        public Guid SecretStorageId { get; set; }
        public SecretStorage SecretStorage { get; set; }

        public Organization()
        {
            OrganizationCreated = DateTime.UtcNow;
        }
    }
}