﻿using System.ComponentModel.DataAnnotations;

namespace CloudelioApi.Entities
{
    public class CloudItemProviderType
    {
        [Key]
        public Guid ItemTypeId { get; set; }

        public string ItemTypeName { get; set; }
        public string ItemTypeDescription { get; set; }
        // array of string purposes as string in DB
        public string RequiredSecretPurposes { get; set; }
    }
}