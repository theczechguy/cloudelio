﻿namespace CloudelioApi.Entities
{
    public class CloudItemProviderSecret
    {
        public Guid Id { get; set; }
        public Guid SecretId { get; set; }
        public Secret Secret { get; set; }
        public Guid CloudItemProviderId { get; set; }
        public CloudItemProvider CloudItemProvider { get; set; }
    }
}