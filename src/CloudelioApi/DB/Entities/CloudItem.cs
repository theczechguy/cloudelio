﻿using CloudelioApi.Entities;
using CloudelioApi.Entities.Models;
using System.ComponentModel.DataAnnotations;

namespace CloudelioApi.DB.Entities
{
    public class CloudItem
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid OrganizationId { get; set; }
        public CloudItemProvider CloudItemProvider { get; set; }
        public Guid CloudItemProviderId { get; set; }
        public CloudGroup CloudGroup { get; set; }
        public Guid CloudGroupId { get; set; }
        [MaxLength(4000)]
        public string Properties { get; set; }
    }
}