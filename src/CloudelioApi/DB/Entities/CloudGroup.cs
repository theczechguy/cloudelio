﻿using System.ComponentModel.DataAnnotations;

namespace CloudelioApi.Entities.Models
{
    public class CloudGroup
    {
        [Key]
        public Guid CloudGroupId { get; set; }

        public string CloudGroupName { get; set; }
        public string? CloudGroupDescription { get; set; }
        public Guid OrganizationId { get; set; }
        public Organization Organization { get; set; }
    }
}