﻿namespace CloudelioApi.Entities
{
    public class SecretStorage
    {
        public Guid SecretStorageId { get; set; }
        public String VaultUri { get; set; }
    }
}