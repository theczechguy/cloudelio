﻿using CloudelioApi.Entities.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CloudelioApi.Entities

{
    public class CloudItemProvider
    {
        [Key]
        public Guid Id { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public Guid OrganizationId { get; set; }
        public Organization Organization { get; set; }

        // https://docs.microsoft.com/en-us/sql/relational-databases/json/store-json-documents-in-sql-tables?view=sql-server-ver15#classic-tables
        [MaxLength(4000)]
        public string Properties { get; set; }

        [ForeignKey("CloudItemType")]
        public Guid ItemTypeId { get; set; }

        public CloudItemProviderType CloudItemType { get; set; }
    }
}