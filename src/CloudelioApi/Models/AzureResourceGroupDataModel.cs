﻿using Azure.Core;

namespace CloudelioApi.Models
{
    public class AzureResourceGroupDataModel
    {
        public string Name { get; set; }
        public string SubscriptionId { get; set; }
        public AzureLocation AzureLocation { get; set; }
    }
}