﻿namespace CloudelioApi.Models
{
    public class AzureCredentialObjectModel
    {
        public string TenantId { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
    }
}