﻿namespace CloudelioApi.DTOs.CloudItemProvider
{
    public class NewCloudItemProviderDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public Guid ItemTypeId { get; set; }
        public Guid OrganizationId { get; set; }
        public Dictionary<string, string> Properties { get; set; }
        public List<Guid> SecretIds { get; set; }
    }
}