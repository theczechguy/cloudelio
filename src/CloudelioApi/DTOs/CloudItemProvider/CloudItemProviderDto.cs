﻿using CloudelioApi.DTOs.ParentClasses;

namespace CloudelioApi.DTOs.CloudItemProvider
{
    public class CloudItemProviderDto : ModelWithProperties
    {
        public Guid Id { get; set; }
        public Guid OrganizationId { get; set; }
        public Guid ItemTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}