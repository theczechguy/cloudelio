﻿namespace CloudelioApi.DTOs.Secret
{
    public class SecretDto
    {
        public Guid SecretId { get; set; }
        public string SecretName { get; set; }
        public Guid OrganizationId { get; set; }
        public string SecretPurpose { get; set; }
    }
}