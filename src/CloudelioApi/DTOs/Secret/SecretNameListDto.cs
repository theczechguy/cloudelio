﻿namespace CloudelioApi.DTOs.Secret
{
    public class SecretNameListDto
    {
        public string SecretName { get; set; }
        public Guid SecretId { get; set; }
    }
}