﻿namespace CloudelioApi.DTOs.Secret
{
    public class SecretWithValueDto
    {
        public Guid SecretId { get; set; }
        public string SecretName { get; set; }
        public string SecretValue { get; set; }
        public Guid OrganizationId { get; set; }
        public string SecretPurpose { get; set; }
    }
}