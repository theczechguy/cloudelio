﻿namespace CloudelioApi.DTOs.Secret
{
    public class DeleteSecretDto
    {
        public Guid SecretId { get; set; }
        public Guid OrganizationId { get; set; }
    }
}