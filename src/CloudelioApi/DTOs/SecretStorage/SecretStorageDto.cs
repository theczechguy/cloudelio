﻿namespace CloudelioApi.DTOs.SecretStorage
{
    public class SecretStorageDto
    {
        public Guid SecretStorageId { get; set; }
        public string VaultUri { get; set; }
    }
}