﻿namespace CloudelioApi.DTOs.SecretStorage
{
    public class RegisterSecretStorageDto
    {
        public string VaultUri { get; set; }
    }
}