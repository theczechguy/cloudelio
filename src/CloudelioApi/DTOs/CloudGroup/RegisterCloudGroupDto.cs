﻿namespace CloudelioApi.DTOs.CloudGroup
{
    public class RegisterCloudGroupDto
    {
        public string CloudGroupName { get; set; }
        public string? CloudGroupDescription { get; set; }
        public Guid OrganizationId { get; set; }
    }
}