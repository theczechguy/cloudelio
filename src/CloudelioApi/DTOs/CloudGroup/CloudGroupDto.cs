﻿namespace CloudelioApi.DTOs.CloudGroup
{
    public class CloudGroupDto
    {
        public Guid CloudGroupId { get; set; }
        public string CloudGroupName { get; set; }
        public string? CloudGroupDescription { get; set; }
    }
}