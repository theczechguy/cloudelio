﻿namespace CloudelioApi.DTOs.CloudItemType
{
    public class CloudItemProviderTypeDto
    {
        public Guid ItemTypeId { get; set; }

        public string ItemTypeName { get; set; }
        public string ItemTypeDescription { get; set; }
        public string RequiredSecretPurposes { get; set; }
    }
}