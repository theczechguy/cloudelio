﻿namespace CloudelioApi.DTOs.CloudItemProviderType
{
    public class RegisterCloudItemProviderTypeDto
    {
        public string ItemTypeName { get; set; }
        public string ItemTypeDescription { get; set; }
        public string[] RequiredSecretPurposes { get; set; }
    }
}