﻿namespace CloudelioApi.DTOs.Organization
{
    public class OrganizationDto
    {
        public Guid OrganizationId { get; set; }
        public string OrganizationName { get; set; }
        public string OrganizationOwner { get; set; }
        public DateTime OrganizationCreated { get; set; }
    }
}