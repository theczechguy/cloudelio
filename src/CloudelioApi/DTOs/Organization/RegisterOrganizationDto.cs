﻿namespace CloudelioApi.DTOs.Organization
{
    public class RegisterOrganizationDto
    {
        public string OrganizationName { get; set; }

        public string OrganizationOwner { get; set; }
    }
}