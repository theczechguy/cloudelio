﻿namespace CloudelioApi.DTOs.CloudItem.Azure
{
    public class ResourceGroupDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        public string ProvisioningState { get; set; }
        public Guid ItemId { get; set; }
    }
}
