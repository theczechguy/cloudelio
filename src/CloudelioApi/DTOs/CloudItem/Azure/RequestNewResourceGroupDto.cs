﻿namespace CloudelioApi.DTOs.CloudItem.Azure
{
    public class RequestNewResourceGroupDto : NewCloudItemBase
    {
        public string Location { get; set; }
    }
}