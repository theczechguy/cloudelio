﻿using CloudelioApi.DTOs.ParentClasses;

namespace CloudelioApi.DTOs.CloudItem
{
    public class CloudItemDto : ModelWithProperties
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid OrganizationId { get; set; }
        public Guid CloudItemProviderId { get; set; }
    }
}