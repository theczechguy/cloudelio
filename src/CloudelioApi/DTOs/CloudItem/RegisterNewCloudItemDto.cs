﻿namespace CloudelioApi.DTOs.CloudItem
{
    public class RegisterNewCloudItemDto
    {
        public string Name { get; set; }
        public Guid OrganizationId { get; set; }
        public Guid CloudItemProviderId { get; set; }
        public Guid CloudGroupId { get; set; }
        public Dictionary<string, string> Properties { get; set; }
    }
}