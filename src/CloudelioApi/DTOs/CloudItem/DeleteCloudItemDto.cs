﻿namespace CloudelioApi.DTOs.CloudItem
{
    public class DeleteCloudItemDto
    {
        public Guid CloudItemId { get; set; }
        public Guid OrganizationId { get; set; }
    }
}
