﻿namespace CloudelioApi.DTOs.CloudItem
{
    public class NewCloudItemBase
    {
        public string Name { get; set; }
        public Guid OrganizationId { get; set; }
        public Guid CloudGroupId { get; set; }
        public Guid CloudItemProviderId { get; set; }
    }
}