﻿namespace CloudelioApi.DTOs.AzureOrchestrator
{
    public class RequestNewResourceGroupDtoOld
    {
        public string Name { get; set; }
        public string AzureLocation { get; set; }
    }
}