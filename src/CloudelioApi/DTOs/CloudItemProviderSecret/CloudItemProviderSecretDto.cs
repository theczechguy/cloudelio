﻿namespace CloudelioApi.DTOs.CloudItemProviderSecret
{
    public class CloudItemProviderSecretDto
    {
        public Guid Id { get; set; }
        public Guid SecretId { get; set; }
        public Guid CloudItemProviderId { get; set; }
    }
}