﻿using System.Text.Json;

namespace CloudelioApi.DTOs.ParentClasses
{
    public class ModelWithProperties
    {
        public string Properties { get; set; }
        /// <summary>
        /// Deserialize string properties into String,String Dictionary
        /// </summary>
        public Dictionary<string, string>? DeserializeProperties()
        {
            if (Properties is not null)
            {
                return JsonSerializer.Deserialize<Dictionary<string, string>>(this.Properties);
            }
            return null;
        }
    }
}