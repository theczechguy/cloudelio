﻿using AutoMapper;
using CloudelioApi.DTOs.Organization;
using CloudelioApi.Entities.Models;

namespace CloudelioApi.AutoMapper
{
    public class OrganizationProfile : Profile
    {
        public OrganizationProfile()
        {
            CreateMap<OrganizationDto, Organization>().ReverseMap();
            CreateMap<RegisterOrganizationDto, Organization>();
        }
    }
}