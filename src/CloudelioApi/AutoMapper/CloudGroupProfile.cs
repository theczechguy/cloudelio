﻿using AutoMapper;
using CloudelioApi.DTOs.CloudGroup;
using CloudelioApi.Entities.Models;

namespace CloudelioApi.AutoMapper
{
    public class CloudGroupProfile : Profile
    {
        public CloudGroupProfile()
        {
            CreateMap<CloudGroupDto, CloudGroup>().ReverseMap();
            CreateMap<RegisterCloudGroupDto, CloudGroup>();
        }
    }
}