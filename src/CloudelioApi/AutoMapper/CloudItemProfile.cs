﻿using AutoMapper;
using CloudelioApi.DB.Entities;
using CloudelioApi.DTOs.CloudItem;

namespace CloudelioApi.AutoMapper
{
    public class CloudItemProfile : Profile
    {
        public CloudItemProfile()
        {
            CreateMap<CloudItem, CloudItemDto>();
        }
    }
}
