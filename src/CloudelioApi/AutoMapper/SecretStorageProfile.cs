﻿using AutoMapper;
using CloudelioApi.DTOs.SecretStorage;
using CloudelioApi.Entities;

namespace CloudelioApi.AutoMapper
{
    public class SecretStorageProfile : Profile
    {
        public SecretStorageProfile()
        {
            CreateMap<SecretStorageDto, SecretStorage>().ReverseMap();
        }
    }
}