﻿using AutoMapper;
using CloudelioApi.DTOs.CloudItemProviderType;
using CloudelioApi.DTOs.CloudItemType;
using CloudelioApi.Entities;

namespace CloudelioApi.AutoMapper
{
    public class CloudItemProviderTypeProfile : Profile
    {
        public CloudItemProviderTypeProfile()
        {
            //CreateMap<CloudGroupDto, CloudGroup>().ReverseMap();
            CreateMap<CloudItemProviderTypeDto, CloudItemProviderType>().ReverseMap();
            CreateMap<RegisterCloudItemProviderTypeDto, CloudItemProviderType>()
                .ForMember(
                    dest => dest.RequiredSecretPurposes,
                    opt => opt.MapFrom(src => string.Join(',', src.RequiredSecretPurposes))
                );
        }
    }
}