﻿using AutoMapper;
using CloudelioApi.DTOs.CloudItemProvider;
using CloudelioApi.Entities;
using System.Text.Json;

namespace CloudelioApi.AutoMapper
{
    public class CloudItemProviderProfile : Profile
    {
        //public CloudItemProviderProfile()
        //{
        //    CreateMap<CloudItemProviderDto, CloudItemProvider>();
        //    CreateMap<CloudItemProvider, CloudItemProviderDto>().ForMember(
        //        dest => dest.Properties,
        //        opt => opt.ConvertUsing(new PropertiesDeserializerConverter())
        //    );
        //}
    }

    public class PropertiesDeserializerConverter : IValueConverter<string, Dictionary<string, string>>
    {
        public Dictionary<string, string> Convert(string sourceMember, ResolutionContext context)
        {
            if (sourceMember is not null)
            {
                return JsonSerializer.Deserialize<Dictionary<string, string>>(sourceMember);
            }
            return null;
        }
    }
}