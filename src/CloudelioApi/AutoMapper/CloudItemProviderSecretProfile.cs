﻿using AutoMapper;
using CloudelioApi.DTOs.CloudItemProviderSecret;
using CloudelioApi.Entities;

namespace CloudelioApi.AutoMapper
{
    public class CloudItemProviderSecretProfile : Profile
    {
        public CloudItemProviderSecretProfile()
        {
            CreateMap<CloudItemProviderSecretDto, CloudItemProviderSecret>().ReverseMap();
        }
    }
}
