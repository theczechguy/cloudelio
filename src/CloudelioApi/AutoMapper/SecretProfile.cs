﻿using AutoMapper;
using CloudelioApi.DTOs.Secret;
using CloudelioApi.Entities;

namespace CloudelioApi.AutoMapper
{
    public class SecretProfile : Profile
    {
        public SecretProfile()
        {
            CreateMap<SecretDto, Secret>().ReverseMap();
            CreateMap<RegisterSecretDto, Secret>();
            CreateMap<Secret, SecretNameListDto>().ReverseMap();
        }
    }
}