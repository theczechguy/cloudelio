﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CloudelioApi.Migrations
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CloudItemTypes",
                columns: table => new
                {
                    ItemTypeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ItemTypeName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ItemTypeDescription = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CloudItemTypes", x => x.ItemTypeId);
                });

            migrationBuilder.CreateTable(
                name: "SecretStorages",
                columns: table => new
                {
                    SecretStorageId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    VaultUri = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SecretStorages", x => x.SecretStorageId);
                });

            migrationBuilder.CreateTable(
                name: "Organizations",
                columns: table => new
                {
                    OrganizationId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    OrganizationName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    OrganizationOwner = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    OrganizationCreated = table.Column<DateTime>(type: "datetime2", nullable: false),
                    SecretStorageId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Organizations", x => x.OrganizationId);
                    table.ForeignKey(
                        name: "FK_Organizations_SecretStorages_SecretStorageId",
                        column: x => x.SecretStorageId,
                        principalTable: "SecretStorages",
                        principalColumn: "SecretStorageId");
                });

            migrationBuilder.CreateTable(
                name: "CloudGroups",
                columns: table => new
                {
                    CloudGroupId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CloudGroupName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CloudGroupDescription = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    OrganizationId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CloudGroups", x => x.CloudGroupId);
                    table.ForeignKey(
                        name: "FK_CloudGroups_Organizations_OrganizationId",
                        column: x => x.OrganizationId,
                        principalTable: "Organizations",
                        principalColumn: "OrganizationId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CloudItemProviders",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    OrganizationId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Properties = table.Column<string>(type: "nvarchar(4000)", maxLength: 4000, nullable: false),
                    ItemTypeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CloudItemProviders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CloudItemProviders_CloudItemTypes_ItemTypeId",
                        column: x => x.ItemTypeId,
                        principalTable: "CloudItemTypes",
                        principalColumn: "ItemTypeId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CloudItemProviders_Organizations_OrganizationId",
                        column: x => x.OrganizationId,
                        principalTable: "Organizations",
                        principalColumn: "OrganizationId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Secrets",
                columns: table => new
                {
                    SecretId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SecretName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    KeyVaultSecretName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    OrganizationId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SecretStorageId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Secrets", x => x.SecretId);
                    table.ForeignKey(
                        name: "FK_Secrets_Organizations_OrganizationId",
                        column: x => x.OrganizationId,
                        principalTable: "Organizations",
                        principalColumn: "OrganizationId");
                    table.ForeignKey(
                        name: "FK_Secrets_SecretStorages_SecretStorageId",
                        column: x => x.SecretStorageId,
                        principalTable: "SecretStorages",
                        principalColumn: "SecretStorageId");
                });

            migrationBuilder.CreateTable(
                name: "CloudItemProvidersSecrets",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SecretId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CloudItemProviderId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CloudItemProvidersSecrets", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CloudItemProvidersSecrets_CloudItemProviders_CloudItemProviderId",
                        column: x => x.CloudItemProviderId,
                        principalTable: "CloudItemProviders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CloudItemProvidersSecrets_Secrets_SecretId",
                        column: x => x.SecretId,
                        principalTable: "Secrets",
                        principalColumn: "SecretId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CloudGroups_OrganizationId",
                table: "CloudGroups",
                column: "OrganizationId");

            migrationBuilder.CreateIndex(
                name: "IX_CloudItemProviders_ItemTypeId",
                table: "CloudItemProviders",
                column: "ItemTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_CloudItemProviders_OrganizationId",
                table: "CloudItemProviders",
                column: "OrganizationId");

            migrationBuilder.CreateIndex(
                name: "IX_CloudItemProvidersSecrets_CloudItemProviderId",
                table: "CloudItemProvidersSecrets",
                column: "CloudItemProviderId");

            migrationBuilder.CreateIndex(
                name: "IX_CloudItemProvidersSecrets_SecretId",
                table: "CloudItemProvidersSecrets",
                column: "SecretId");

            migrationBuilder.CreateIndex(
                name: "IX_Organizations_SecretStorageId",
                table: "Organizations",
                column: "SecretStorageId");

            migrationBuilder.CreateIndex(
                name: "IX_Secrets_OrganizationId",
                table: "Secrets",
                column: "OrganizationId");

            migrationBuilder.CreateIndex(
                name: "IX_Secrets_SecretStorageId",
                table: "Secrets",
                column: "SecretStorageId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CloudGroups");

            migrationBuilder.DropTable(
                name: "CloudItemProvidersSecrets");

            migrationBuilder.DropTable(
                name: "CloudItemProviders");

            migrationBuilder.DropTable(
                name: "Secrets");

            migrationBuilder.DropTable(
                name: "CloudItemTypes");

            migrationBuilder.DropTable(
                name: "Organizations");

            migrationBuilder.DropTable(
                name: "SecretStorages");
        }
    }
}