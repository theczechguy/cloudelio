﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CloudelioApi.Migrations
{
    public partial class secretpurpose : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CloudItemProviderTypes",
                keyColumn: "ItemTypeId",
                keyValue: new Guid("83e33424-c8f2-4400-bc2f-b2a177191278"));

            migrationBuilder.AddColumn<string>(
                name: "SecretPurpose",
                table: "Secrets",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "RequiredSecretPurposes",
                table: "CloudItemProviderTypes",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateTable(
                name: "CloudItems",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    OrganizationId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CloudItemProviderId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CloudGroupId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Properties = table.Column<string>(type: "nvarchar(4000)", maxLength: 4000, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CloudItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CloudItems_CloudGroups_CloudGroupId",
                        column: x => x.CloudGroupId,
                        principalTable: "CloudGroups",
                        principalColumn: "CloudGroupId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CloudItems_CloudItemProviders_CloudItemProviderId",
                        column: x => x.CloudItemProviderId,
                        principalTable: "CloudItemProviders",
                        principalColumn: "Id");
                });

            migrationBuilder.InsertData(
                table: "CloudItemProviderTypes",
                columns: new[] { "ItemTypeId", "ItemTypeDescription", "ItemTypeName", "RequiredSecretPurposes" },
                values: new object[] { new Guid("ccd26f0c-844d-43a1-8cef-90880a3a572f"), "ResourceGroup in Azure subscription", "Resource Group", "TenantId,ClientId,ClientSecret" });

            migrationBuilder.CreateIndex(
                name: "IX_CloudItems_CloudGroupId",
                table: "CloudItems",
                column: "CloudGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_CloudItems_CloudItemProviderId",
                table: "CloudItems",
                column: "CloudItemProviderId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CloudItems");

            migrationBuilder.DeleteData(
                table: "CloudItemProviderTypes",
                keyColumn: "ItemTypeId",
                keyValue: new Guid("ccd26f0c-844d-43a1-8cef-90880a3a572f"));

            migrationBuilder.DropColumn(
                name: "SecretPurpose",
                table: "Secrets");

            migrationBuilder.DropColumn(
                name: "RequiredSecretPurposes",
                table: "CloudItemProviderTypes");

            migrationBuilder.InsertData(
                table: "CloudItemProviderTypes",
                columns: new[] { "ItemTypeId", "ItemTypeDescription", "ItemTypeName" },
                values: new object[] { new Guid("83e33424-c8f2-4400-bc2f-b2a177191278"), "ResourceGroup in Azure subscription", "Resource Group" });
        }
    }
}
