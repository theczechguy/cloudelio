﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CloudelioApi.Migrations
{
    public partial class fixtablename : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CloudItemProviders_CloudItemTypes_ItemTypeId",
                table: "CloudItemProviders");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CloudItemTypes",
                table: "CloudItemTypes");

            migrationBuilder.RenameTable(
                name: "CloudItemTypes",
                newName: "CloudItemProviderTypes");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CloudItemProviderTypes",
                table: "CloudItemProviderTypes",
                column: "ItemTypeId");

            migrationBuilder.InsertData(
                table: "CloudItemProviderTypes",
                columns: new[] { "ItemTypeId", "ItemTypeDescription", "ItemTypeName" },
                values: new object[] { new Guid("83e33424-c8f2-4400-bc2f-b2a177191278"), "ResourceGroup in Azure subscription", "Resource Group" });

            migrationBuilder.AddForeignKey(
                name: "FK_CloudItemProviders_CloudItemProviderTypes_ItemTypeId",
                table: "CloudItemProviders",
                column: "ItemTypeId",
                principalTable: "CloudItemProviderTypes",
                principalColumn: "ItemTypeId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CloudItemProviders_CloudItemProviderTypes_ItemTypeId",
                table: "CloudItemProviders");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CloudItemProviderTypes",
                table: "CloudItemProviderTypes");

            migrationBuilder.DeleteData(
                table: "CloudItemProviderTypes",
                keyColumn: "ItemTypeId",
                keyValue: new Guid("83e33424-c8f2-4400-bc2f-b2a177191278"));

            migrationBuilder.RenameTable(
                name: "CloudItemProviderTypes",
                newName: "CloudItemTypes");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CloudItemTypes",
                table: "CloudItemTypes",
                column: "ItemTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_CloudItemProviders_CloudItemTypes_ItemTypeId",
                table: "CloudItemProviders",
                column: "ItemTypeId",
                principalTable: "CloudItemTypes",
                principalColumn: "ItemTypeId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}