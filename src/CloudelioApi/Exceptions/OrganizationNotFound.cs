﻿namespace CloudelioApi.Exceptions
{
    public class OrganizationNotFound : AppException
    {
        public OrganizationNotFound() : base("Organization not found")
        {
        }
    }
}