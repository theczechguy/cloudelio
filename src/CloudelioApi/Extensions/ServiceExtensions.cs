﻿using CloudelioApi.Entities;
using CloudelioApi.Services;
using CloudelioApi.Services.AzureOrchestration;
using CloudelioApi.Services.CloudItem;
using CloudelioApi.Services.CloudItemProvider;
using CloudelioApi.Services.CloudItemProviderType;
using CloudelioApi.Services.CloudItemType;
using CloudelioApi.Services.Implementations;
using CloudelioApi.Services.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace CloudelioApi.Extensions
{
    public static class ServiceExtensions
    {
        public static void ConfigureSqlServerContext(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<DataContext>(o => o.UseSqlServer(configuration["CloudelioAzureSQL"]));
        }

        public static void ConfigureAutoMapper(this IServiceCollection services)
        {
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
        }

        public static void ConfigureAppServices(this IServiceCollection services)
        {
            services.AddScoped<ICloudGroupService, CloudGroupService>();
            services.AddScoped<IOrganizationService, OrganizationService>();
            services.AddScoped<ISecretStorageService, SecretStorageService>();
            services.AddScoped<IAzureOrchestratorService, AzureOrchestratorService>();
            services.AddScoped<ICloudItemProviderService, CloudItemProviderService>();
            services.AddScoped<ICloudItemProviderTypeService, CloudItemProviderTypeService>();
            services.AddScoped<ICloudItemService, CloudItemService>();

            //services.AddScoped<ISecretService, FakeSecretService>();
            services.AddScoped<ISecretService, AzureSecretService>();
        }
    }
}