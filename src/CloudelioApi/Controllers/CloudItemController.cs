using CloudelioApi.DTOs.CloudItem;
using CloudelioApi.DTOs.CloudItem.Azure;
using CloudelioApi.Services.CloudItem;
using Microsoft.AspNetCore.Mvc;

namespace CloudelioApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CloudItemController : Controller
    {
        private readonly ICloudItemService _cloudItemService;

        public CloudItemController(ICloudItemService cloudItemService)
        {
            _cloudItemService = cloudItemService;
        }

        [HttpPost("azure/resourceGroup")]
        public async Task<IActionResult> CreateAzureRG([FromBody] RequestNewResourceGroupDto registerNewCloudItemDto)
        {
            var newRg = await _cloudItemService.CreateNewAzureResourceGroup(registerNewCloudItemDto);
            return Ok(newRg);
        }

        [HttpDelete("azure/resourceGroup")]
        public async Task<IActionResult> DeleteAzureRG([FromBody] DeleteCloudItemDto deleteCloudItemDto)
        {
            await _cloudItemService.DeleteAzureResourceGroup(deleteCloudItemDto);
            return Ok();
        }
    }
}