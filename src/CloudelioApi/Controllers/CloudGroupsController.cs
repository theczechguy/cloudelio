﻿using AutoMapper;
using CloudelioApi.DTOs.CloudGroup;
using CloudelioApi.Services;
using Microsoft.AspNetCore.Mvc;

namespace CloudelioApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CloudGroupsController : Controller
    {
        private readonly ICloudGroupService _cloudGroupService;
        private readonly IOrganizationService _organizationService;

        private readonly ILogger<OrganizationsController> _logger;
        private readonly IMapper _mapper;

        public CloudGroupsController(
            ICloudGroupService cloudGroupService,
            IOrganizationService organizationService,
            ILogger<OrganizationsController> logger,
            IMapper mapper)
        {
            _cloudGroupService = cloudGroupService;
            _organizationService = organizationService;
            _logger = logger;
            _mapper = mapper;
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] RegisterCloudGroupDto registerCloudGroupDto)
        {
            var newCloudGroup = await _cloudGroupService.AddCloudGroupAsync(registerCloudGroupDto);
            return Ok(newCloudGroup);
        }

        [HttpDelete("{cloudGroupId}")]
        public async Task<IActionResult> Delete(Guid cloudGroupId)
        {
            await _cloudGroupService.DeleteCloudGroupAsync(cloudGroupId);
            return Ok("CloudGroup deleted");
        }

        [HttpGet("{organizationId}")]
        public async Task<IActionResult> GetCloudGroupsFromOrganization(Guid organizationId)
        {
            var cloudGroupDtos = await _organizationService.GetCloudGroupsByOrgId(organizationId);
            return Ok(cloudGroupDtos);
        }

        [HttpGet()]
        public async Task<IActionResult> GetCloudGroupsByName([FromQuery] string name)
        {
            var cloudGroupDtos = await _cloudGroupService.FindCloudGroupByNameAsync(name);
            return Ok(cloudGroupDtos);
        }
    }
}