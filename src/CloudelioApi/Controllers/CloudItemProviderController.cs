﻿using CloudelioApi.DTOs.CloudItemProvider;
using CloudelioApi.Services.CloudItemProvider;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CloudelioApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CloudItemProviderController : ControllerBase
    {
        private readonly ICloudItemProviderService _cloudItemProvider;

        public CloudItemProviderController(ICloudItemProviderService cloudItemProvider)
        {
            _cloudItemProvider = cloudItemProvider;
        }

        [HttpPost]
        public async Task<ActionResult> RegisterItemProvider([FromBody] NewCloudItemProviderDto newCloudItemProviderDto)
        {
            var newProviderName = await _cloudItemProvider.CreateNewCloudItemProviderAsync(newCloudItemProviderDto);
            return Ok(newProviderName);
        }

        [HttpGet]
        public async Task<IActionResult> GetCloudItemProvider([FromQuery] Guid id, Guid organizationId)
        {
            var itemProvider = await _cloudItemProvider.GetCloudItemProviderByIdAsync(id, organizationId);
            return Ok(itemProvider);
        }
    }
}