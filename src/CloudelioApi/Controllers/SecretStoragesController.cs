﻿using AutoMapper;
using CloudelioApi.DTOs.SecretStorage;
using CloudelioApi.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace CloudelioApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SecretStoragesController : Controller
    {
        private readonly ISecretStorageService _secretStorageService;
        private readonly IMapper _mapper;

        public SecretStoragesController(ISecretStorageService secretStorageService, IMapper mapper)
        {
            _secretStorageService = secretStorageService;
            _mapper = mapper;
        }

        [HttpPost()]
        public async Task<IActionResult> AddSecretStorage([FromBody] RegisterSecretStorageDto registerSecretStorageDto)
        {
            var newSecretStorageDto = await _secretStorageService.AddSecretStorage(registerSecretStorageDto.VaultUri);
            return Ok(newSecretStorageDto);
        }

        [HttpDelete("{storageId}")]
        public async Task<IActionResult> RemoveSecretStorage(Guid storageId)
        {
            var storageIdResult = await _secretStorageService.RemoveSecretStorage(storageId);
            return Ok(storageIdResult);
        }

        [HttpGet()]
        public async Task<IActionResult> ListSecretStoragesForOrg()
        {
            var allSecretStorages = await _secretStorageService.GetSecretStorages();
            return Ok(allSecretStorages);
        }
    }
}