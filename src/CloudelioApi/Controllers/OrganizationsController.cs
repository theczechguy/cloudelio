﻿using AutoMapper;
using CloudelioApi.DTOs.Organization;
using CloudelioApi.DTOs.Secret;
using CloudelioApi.Services;
using CloudelioApi.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace CloudelioApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrganizationsController : Controller
    {
        private readonly IOrganizationService _organizationService;
        private readonly ISecretService _secretService;
        private readonly ISecretStorageService _secretStorageService;
        private readonly IMapper _mapper;

        public OrganizationsController(
            IOrganizationService organizationService,
            ISecretService secretService,
            ISecretStorageService secretStorageService,
            IMapper mapper)
        {
            _organizationService = organizationService;
            _secretService = secretService;
            _secretStorageService = secretStorageService;
            _mapper = mapper;
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] RegisterOrganizationDto registerOrganizationDto)
        {
            var newOrganization = await _organizationService.AddOrganizationAsync(registerOrganizationDto);
            return CreatedAtAction(nameof(GetOrganization), new { guid = newOrganization.OrganizationId }, newOrganization);
        }

        [HttpDelete("{OrganizationId}")]
        public async Task<IActionResult> Delete(Guid OrganizationId)
        {
            await _organizationService.DeleteOrganizationAsync(OrganizationId);
            return Ok("Organization deleted");
        }

        [HttpGet()]
        public async Task<IActionResult> GetOrganization([FromQuery] Guid guid, [FromQuery] string? name)
        {
            if (guid != Guid.Empty)
            {
                var organization = await _organizationService.FindOrganizationByIdAsync(guid);
                return Ok(organization);
            }
            if (!string.IsNullOrWhiteSpace(name))
            {
                var organization = await _organizationService.FindOrganizationByNameAsync(name);
                return Ok(organization);
            }
            return BadRequest("Unable to find organization with given parameters");
        }

        [HttpGet("{guid}/cloudgroup")]
        public async Task<IActionResult> GetCloudGroupsFromOrganization(Guid guid)
        {
            var cloudGroupDtos = await _organizationService.GetCloudGroupsByOrgId(guid);
            return Ok(cloudGroupDtos);
        }

        [HttpPost("{OrganizationId}/secrets")]
        public async Task<IActionResult> CreateSecret([FromBody] RegisterSecretDto registerSecretDto)
        {
            var newSecretDto = await _secretService.CreateNewSecret(registerSecretDto);
            return Ok(newSecretDto);
        }

        [HttpGet("{OrganizationId}/secrets")]
        public async Task<IActionResult> GetSecret([FromQuery] Guid secretId, Guid OrganizationId)
        {
            var secretDto = await _secretService.GetSecretById(secretId, OrganizationId);
            return Ok(secretDto);
        }

        [HttpDelete("{OrganizationId}/secrets/{secretId}")]
        public async Task<IActionResult> DeleteSecret(Guid OrganizationId, Guid secretId)
        {
            var deleteDto = new DeleteSecretDto
            {
                OrganizationId = OrganizationId,
                SecretId = secretId
            };
            _ = await _secretService.DeleteSecret(deleteDto);
            return Ok();
        }

        [HttpGet("{OrganizationId}/secrets/listAll")]
        public async Task<IActionResult> ListAllOrgSecretNames(Guid OrganizationId)
        {
            var allSecretNames = await _secretService.ListAllSecrets(OrganizationId);
            return Ok(allSecretNames);
        }
    }
}