﻿using CloudelioApi.DTOs.CloudItemProviderType;
using CloudelioApi.Services.CloudItemProviderType;
using Microsoft.AspNetCore.Mvc;

namespace CloudelioApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CloudItemProviderTypesController : Controller
    {
        private readonly ICloudItemProviderTypeService _cloudItemTypeService;

        public CloudItemProviderTypesController(ICloudItemProviderTypeService cloudItemTypeService)
        {
            _cloudItemTypeService = cloudItemTypeService;
        }

        [HttpGet]
        public async Task<IActionResult> GetItemTypes()
        {
            return Ok(await _cloudItemTypeService.GetAllCloudItemTypes());
        }

        [HttpGet("{providerId}")]
        public async Task<IActionResult> GetProviderTypeById(Guid providerId)
        {
            return Ok(await _cloudItemTypeService.GetItemProviderTypeById(providerId));
        }

        [HttpPost]
        public async Task<IActionResult> NewCloudItemProviderType([FromBody] RegisterCloudItemProviderTypeDto registerCloudItemProviderTypeDto)
        {
            return Ok(await _cloudItemTypeService.RegisterCloudItemProviderType(registerCloudItemProviderTypeDto));
        }
    }
}