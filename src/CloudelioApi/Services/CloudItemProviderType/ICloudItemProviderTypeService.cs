﻿using CloudelioApi.DTOs.CloudItemProviderType;
using CloudelioApi.DTOs.CloudItemType;

namespace CloudelioApi.Services.CloudItemProviderType
{
    public interface ICloudItemProviderTypeService
    {
        Task<List<CloudItemProviderTypeDto>> GetAllCloudItemTypes();

        Task<CloudItemProviderTypeDto> RegisterCloudItemProviderType(RegisterCloudItemProviderTypeDto registerCloudItemProviderTypeDto);

        Task<CloudItemProviderTypeDto> GetItemProviderTypeById(Guid guid);
    }
}