﻿using AutoMapper;
using CloudelioApi.DTOs.CloudItemProviderType;
using CloudelioApi.DTOs.CloudItemType;
using CloudelioApi.Entities;
using CloudelioApi.Services.CloudItemProviderType;
using Microsoft.EntityFrameworkCore;

namespace CloudelioApi.Services.CloudItemType
{
    public class CloudItemProviderTypeService : ICloudItemProviderTypeService
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        public CloudItemProviderTypeService(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        public async Task<List<CloudItemProviderTypeDto>> GetAllCloudItemTypes()
        {
            return _mapper.Map<List<CloudItemProviderTypeDto>>(await _dataContext.CloudItemProviderTypes.ToListAsync());
        }

        public async Task<CloudItemProviderTypeDto> GetItemProviderTypeById(Guid providerId)
        {
            var provider = await _dataContext.CloudItemProviderTypes.FirstOrDefaultAsync(pt => pt.ItemTypeId == providerId);
            return _mapper.Map<CloudItemProviderTypeDto>(provider);
        }

        public async Task<CloudItemProviderTypeDto> RegisterCloudItemProviderType(RegisterCloudItemProviderTypeDto registerCloudItemProviderTypeDto)
        {
            var objectToRegister = _mapper.Map<Entities.CloudItemProviderType>(registerCloudItemProviderTypeDto);
            _dataContext.CloudItemProviderTypes.Add(objectToRegister);
            await _dataContext.SaveChangesAsync();
            return _mapper.Map<CloudItemProviderTypeDto>(objectToRegister);
        }
    }
}