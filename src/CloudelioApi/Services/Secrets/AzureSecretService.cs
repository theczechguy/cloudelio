﻿using AutoMapper;
using Azure.Core;
using Azure.Identity;
using Azure.Security.KeyVault.Secrets;
using CloudelioApi.DTOs.Secret;
using CloudelioApi.Entities;
using CloudelioApi.Exceptions;
using CloudelioApi.Services.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace CloudelioApi.Services.Implementations
{
    public class AzureSecretService : ISecretService
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;
        private readonly ISecretStorageService _secretStorageService;
        private readonly ILogger<AzureSecretService> _logger;

        public AzureSecretService(DataContext dataContext, IMapper mapper, ISecretStorageService secretStorageService, ILogger<AzureSecretService> logger)
        {
            _dataContext = dataContext;
            _mapper = mapper;
            _secretStorageService = secretStorageService;
            _logger = logger;
        }

        public async Task<SecretDto> CreateNewSecret(RegisterSecretDto secret)
        {
            var org = await _dataContext.Organizations.AsNoTracking()
                .Where(o => o.OrganizationId == secret.OrganizationId)
                .Include(o => o.SecretStorage)
                .FirstOrDefaultAsync();

            var vaultClient = _createSecretClientAsync(org.SecretStorage.VaultUri);
            var secretName = _generateSecretName(secret.OrganizationId, secret.SecretName);

            var result = await vaultClient.SetSecretAsync(secretName, secret.SecretValue);

            var secretEntity = _mapper.Map<Secret>(secret);
            secretEntity.KeyVaultSecretName = secretName;
            secretEntity.SecretStorageId = org.SecretStorage.SecretStorageId;

            _ = _dataContext.Secrets.Add(secretEntity);
            _ = await _dataContext.SaveChangesAsync();
            return _mapper.Map<SecretDto>(secretEntity);
        }

        public async Task<bool> DeleteSecret(DeleteSecretDto deleteSecretDto)
        {
            var secretWithStorage = await _GetSecretEntityWithStorageById(deleteSecretDto.SecretId,
                                                                deleteSecretDto.OrganizationId);
            var vaultClient = _createSecretClientAsync(secretWithStorage.SecretStorage.VaultUri);

            _logger.LogDebug("Starting delete on secret: {@secretName}", secretWithStorage.KeyVaultSecretName);
            var deleteResult = await vaultClient.StartDeleteSecretAsync(secretWithStorage.KeyVaultSecretName);

            _logger.LogDebug("Waiting for secret delete completion");
            await deleteResult.WaitForCompletionAsync();

            _logger.LogDebug("Removing secret from database");
            _dataContext.Secrets.Remove(secretWithStorage);
            await _dataContext.SaveChangesAsync();

            return true;
        }

        // makes sense to either modify this method or create another one for obtaining multiple secrets in one call
        // this would save some time as the client would be setup only once
        public async Task<SecretWithValueDto> GetSecretById(Guid secretId, Guid organizationId)
        {
            var secret = await this._GetSecretEntityWithStorageById(secretId, organizationId);

            var vaultClient = _createSecretClientAsync(secret.SecretStorage.VaultUri);
            var secretContent = await vaultClient.GetSecretAsync(secret.KeyVaultSecretName);

            return new SecretWithValueDto
            {
                OrganizationId = secret.OrganizationId,
                SecretId = secret.SecretId,
                SecretName = secret.SecretName,
                SecretValue = secretContent.Value.Value,
                SecretPurpose = secret.SecretPurpose
            };
        }

        public async Task<List<SecretWithValueDto>> GetSecretsWithValueById(Guid[] secretIds, Guid organizationId)
        {
            var secretEntities = new List<Secret>();
            foreach (var secretId in secretIds)
            {
                secretEntities.Add(await _GetSecretEntityWithStorageById(secretId, organizationId));
            }
            var vaultClient = _createSecretClientAsync(secretEntities.First().SecretStorage.VaultUri);

            var secretsWithValuesDto = new List<SecretWithValueDto>();
            foreach (var secretEntity in secretEntities)
            {
                var secretContent = await vaultClient.GetSecretAsync(secretEntity.KeyVaultSecretName);
                secretsWithValuesDto.Add(new SecretWithValueDto
                {
                    OrganizationId = secretEntity.OrganizationId,
                    SecretId = secretEntity.SecretId,
                    SecretName = secretEntity.SecretName,
                    SecretPurpose = secretEntity.SecretPurpose,
                    SecretValue = secretContent.Value.Value
                });
            }
            return secretsWithValuesDto;
        }

        public async Task<SecretWithValueDto> GetSecretByName(string secretName, Guid organizationId)
        {
            var secret = await this._GetSecretEntityWithStorageByName(secretName, organizationId);

            var vaultClient = _createSecretClientAsync(secret.SecretStorage.VaultUri);
            var secretContent = await vaultClient.GetSecretAsync(secret.KeyVaultSecretName);

            return new SecretWithValueDto
            {
                OrganizationId = secret.OrganizationId,
                SecretId = secret.SecretId,
                SecretName = secret.SecretName,
                SecretValue = secretContent.Value.Value
            };
        }

        public async Task<List<SecretWithValueDto>> GetSecretsWithValueForCloudItemProvider(Guid organizationId, Guid cloudItemProviderId)
        {
            var secrets = await this._getSecretEntitiesWithStorageByCipId(organizationId, cloudItemProviderId);
            var vaultClient = _createSecretClientAsync(secrets.First().SecretStorage.VaultUri);
            List<SecretWithValueDto> secretValues = new List<SecretWithValueDto>();
            foreach (var secretEntity in secrets)
            {
                var secretContent = await vaultClient.GetSecretAsync(secretEntity.KeyVaultSecretName);
                secretValues.Add(new SecretWithValueDto
                {
                    OrganizationId = organizationId,
                    SecretId = secretEntity.SecretId,
                    SecretName = secretEntity.SecretName,
                    SecretValue = secretContent.Value.Value
                });
            }
            return secretValues;
        }

        public async Task<List<SecretNameListDto>> ListAllSecrets(Guid organizationId)
        {
            var allSecrets = await _dataContext.Secrets.AsNoTracking()
                .Where(s => s.OrganizationId == organizationId)
                .ToListAsync();
            return _mapper.Map<List<SecretNameListDto>>(allSecrets);
        }

        private SecretClient _createSecretClientAsync(string vaultUri)
        {
            // to handle KeyVault throttling
            // https://github.com/MicrosoftDocs/azure-docs/blob/main/articles/key-vault/general/overview-throttling.md
            SecretClientOptions options = new SecretClientOptions()
            {
                Retry =
                {
                    Delay= TimeSpan.FromSeconds(2),
                    MaxDelay = TimeSpan.FromSeconds(16),
                    MaxRetries = 5,
                    Mode = RetryMode.Exponential
                 }
            };
            var clientSecretCredential = new ClientSecretCredential("7e3e5da5-2d32-45ff-941c-9dfe075a3034", "d3edc2b5-d771-44f3-ab72-afd7300fd7ad", "dICe~D7De5kEyKiVP8ZfApDzDj9t0u.3Sc");
            //return new SecretClient(new Uri(vaultUri), new DefaultAzureCredential(), options);
            return new SecretClient(new Uri(vaultUri), clientSecretCredential, options);
        }

        private string _generateSecretName(Guid organizationId, string secretName)
        {
            return $"{organizationId}--{secretName}";
        }

        private async Task<Secret> _GetSecretEntityWithStorageById(Guid secretId, Guid organizationId)
        {
            Secret? secret = await _dataContext.Secrets.AsNoTracking()
                            .Where(s => s.OrganizationId == organizationId && s.SecretId == secretId)
                            .Include(s => s.SecretStorage).FirstOrDefaultAsync();
            if (secret is null)
            {
                throw new AppException($"Unable to find secret with id: {secretId}");
            }
            return secret;
        }

        private async Task<Secret> _GetSecretEntityWithStorageByName(string name, Guid organizationId)
        {
            Secret? secret = await _dataContext.Secrets.AsNoTracking()
                            .Where(s => s.OrganizationId == organizationId && s.SecretName == name)
                            .Include(s => s.SecretStorage).FirstOrDefaultAsync();
            if (secret is null)
            {
                throw new AppException($"Unable to find secret with name: {name}");
            }
            return secret;
        }

        private async Task<List<Secret>> _getSecretEntitiesWithStorageByCipId(Guid cloudItemProviderId, Guid organizationId)
        {
            List<CloudItemProviderSecret> cipSecrets = await _dataContext.CloudItemProvidersSecrets.AsNoTracking()
                .Where(s => s.CloudItemProviderId == cloudItemProviderId).ToListAsync();
            if (cipSecrets is null)
            {
                throw new AppException("Unable to find any secrets for specified provider");
            }
            List<Secret> secrets = new List<Secret>();
            foreach (var cipSecret in cipSecrets)
            {
                var secret = await _dataContext.Secrets.AsNoTracking().Where(sec => sec.OrganizationId == organizationId)
                .Where(sec => sec.SecretId == cipSecret.SecretId).Include(sec => sec.SecretStorage).FirstOrDefaultAsync();
                if (secret is not null)
                {
                    secrets.Add(secret);
                }
            }
            return secrets;
        }
    }
}