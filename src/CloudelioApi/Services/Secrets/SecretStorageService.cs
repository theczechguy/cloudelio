﻿using AutoMapper;
using CloudelioApi.DTOs.SecretStorage;
using CloudelioApi.Entities;
using CloudelioApi.Services.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace CloudelioApi.Services.Implementations
{
    public class SecretStorageService : ISecretStorageService
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        public SecretStorageService(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        public async Task<SecretStorageDto> AddSecretStorage(string vaultUri)
        {
            var newSecretStorage = new SecretStorage
            {
                VaultUri = vaultUri,
            };
            _ = _dataContext.SecretStorages.Add(newSecretStorage);
            _ = await _dataContext.SaveChangesAsync();

            var createdSecretStorage = await _dataContext
                .SecretStorages
                .FirstOrDefaultAsync(
                    s => s.VaultUri == vaultUri
);
            return _mapper.Map<SecretStorageDto>(createdSecretStorage);
        }

        public async Task<List<SecretStorageDto>> GetSecretStorages()
        {
            var allSecretStorages = await _dataContext
                .SecretStorages.ToListAsync();
            return _mapper.Map<List<SecretStorageDto>>(allSecretStorages);
        }

        public async Task<SecretStorageDto> PickOneSecretStorage()
        {
            var allStorages = await this.GetSecretStorages();
            if (allStorages.Count == 1)
            {
                return allStorages[0];
            }
            var random = new Random();
            var selectedStorage = allStorages[random.Next(allStorages.Count)];
            return selectedStorage;
        }

        public async Task<Guid> RemoveSecretStorage(Guid storageId)
        {
            var secretStorage = await _dataContext.SecretStorages
                .FirstOrDefaultAsync(s => s.SecretStorageId == storageId);
            if (secretStorage is null)
            {
                return Guid.Empty;
            }
            _ = _dataContext.SecretStorages.Remove(secretStorage);
            _ = await _dataContext.SaveChangesAsync();
            return secretStorage.SecretStorageId;
        }
    }
}