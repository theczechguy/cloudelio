﻿using CloudelioApi.DTOs.Secret;

namespace CloudelioApi.Services.Interfaces
{
    public interface ISecretService
    {
        Task<SecretDto> CreateNewSecret(RegisterSecretDto secret);

        Task<bool> DeleteSecret(DeleteSecretDto deleteSecretDto);

        Task<SecretWithValueDto> GetSecretById(Guid secretId, Guid organizationId);

        Task<SecretWithValueDto> GetSecretByName(string secretName, Guid organizationId);

        Task<List<SecretNameListDto>> ListAllSecrets(Guid organizationId);

        Task<List<SecretWithValueDto>> GetSecretsWithValueForCloudItemProvider(Guid organizationId, Guid cloudItemProviderId);
        Task<List<SecretWithValueDto>> GetSecretsWithValueById(Guid[] secretIds, Guid organizationId);
    }
}