﻿using CloudelioApi.DTOs.SecretStorage;

namespace CloudelioApi.Services.Interfaces
{
    public interface ISecretStorageService
    {
        Task<Guid> RemoveSecretStorage(Guid storageId);

        Task<List<SecretStorageDto>> GetSecretStorages();

        Task<SecretStorageDto> PickOneSecretStorage();

        Task<SecretStorageDto> AddSecretStorage(string vaultUri);
    }
}