﻿using AutoMapper;
using CloudelioApi.DTOs.CloudGroup;
using CloudelioApi.Entities;
using CloudelioApi.Entities.Models;
using CloudelioApi.Exceptions;
using Microsoft.EntityFrameworkCore;

namespace CloudelioApi.Services
{
    public class CloudGroupService : ICloudGroupService
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        public CloudGroupService(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        public async Task<CloudGroupDto> AddCloudGroupAsync(RegisterCloudGroupDto registerCloudGroupDto)
        {
            var existingCloudGroup = await FindCloudGroupByNameAsync(registerCloudGroupDto.CloudGroupName);
            if (existingCloudGroup is not null)
            {
                throw new AppException("Cloud group name is already in use");
            }

            var cloudGroup = _mapper.Map<CloudGroup>(registerCloudGroupDto);
            _dataContext.CloudGroups.Add(cloudGroup);
            await _dataContext.SaveChangesAsync();

            return _mapper.Map<CloudGroupDto>(cloudGroup);
        }

        public async Task DeleteCloudGroupAsync(Guid cloudGroupId)
        {
            var cloudGroupDto = await FindCloudGroupByIdAsync(cloudGroupId);
            if (cloudGroupDto is not null)
            {
                var cloudGroup = _mapper.Map<CloudGroup>(cloudGroupDto);
                _dataContext.CloudGroups.Remove(cloudGroup);
                await _dataContext.SaveChangesAsync();
            }
        }

        public async Task<CloudGroupDto> FindCloudGroupByIdAsync(Guid cloudGroupId)
        {
            var cloudGroup = await _dataContext.CloudGroups.FindAsync(cloudGroupId);
            if (cloudGroup is null)
            {
                throw new AppException("Cloud group not found");
            }
            return _mapper.Map<CloudGroupDto>(cloudGroup);
        }

        public async Task<CloudGroupDto> FindCloudGroupByNameAsync(string cloudGroupName)
        {
            var cloudGroup = await _dataContext
                .CloudGroups
                .FirstOrDefaultAsync(g => g.CloudGroupName == cloudGroupName);

            return _mapper.Map<CloudGroupDto>(cloudGroup);
        }
    }
}