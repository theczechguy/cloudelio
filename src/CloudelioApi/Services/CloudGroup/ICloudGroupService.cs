﻿using CloudelioApi.DTOs.CloudGroup;

namespace CloudelioApi.Services
{
    public interface ICloudGroupService
    {
        Task<CloudGroupDto> AddCloudGroupAsync(RegisterCloudGroupDto registerCloudGroupDto);

        Task<CloudGroupDto> FindCloudGroupByNameAsync(string cloudGroupName);

        Task DeleteCloudGroupAsync(Guid cloudGroupId);

        Task<CloudGroupDto> FindCloudGroupByIdAsync(Guid cloudGroupId);
    }
}