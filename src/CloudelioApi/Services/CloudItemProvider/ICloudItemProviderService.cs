﻿using CloudelioApi.DTOs.CloudItemProvider;
using CloudelioApi.DTOs.CloudItemProviderSecret;

namespace CloudelioApi.Services.CloudItemProvider
{
    public interface ICloudItemProviderService
    {
        Task<CloudItemProviderDto> CreateNewCloudItemProviderAsync(NewCloudItemProviderDto newCloudItemProviderDto);
        Task<CloudItemProviderDto> GetCloudItemProviderByIdAsync(Guid Id, Guid OrganizationId);
        Task<List<CloudItemProviderSecretDto>> GetCloudItemProviderSecretByProviderIdAsync(Guid ProviderId);
    }
}