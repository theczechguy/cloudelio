﻿using AutoMapper;
using CloudelioApi.DTOs.CloudItemProvider;
using CloudelioApi.DTOs.CloudItemProviderSecret;
using CloudelioApi.Entities;
using CloudelioApi.Exceptions;
using CloudelioApi.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Text.Json;

namespace CloudelioApi.Services.CloudItemProvider
{
    public class CloudItemProviderService : ICloudItemProviderService
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;
        private readonly ISecretService _secretService;

        public CloudItemProviderService(DataContext dataContext, IMapper mapper, ISecretService secretService)
        {
            _dataContext = dataContext;
            _mapper = mapper;
            _secretService = secretService;
        }

        public async Task<CloudItemProviderDto> CreateNewCloudItemProviderAsync(NewCloudItemProviderDto newCloudItemProviderDto)
        {
            var newCloudItemProvider = new Entities.CloudItemProvider
            {
                Id = Guid.NewGuid(),
                Description = newCloudItemProviderDto.Description,
                Name = newCloudItemProviderDto.Name,
                OrganizationId = newCloudItemProviderDto.OrganizationId,
                Properties = JsonSerializer.Serialize(newCloudItemProviderDto.Properties),
                ItemTypeId = newCloudItemProviderDto.ItemTypeId
            };

            // add CIP record
            _dataContext.CloudItemProviders.Add(newCloudItemProvider);

            // get cip type -> required secret purposes
            // get secrets
            // check if all required purposes are found in the list of secrets
            // fail if some purpose is missing
            // create cip+secrets joining table record
            var cipType = await _dataContext.CloudItemProviderTypes.FirstOrDefaultAsync(t => t.ItemTypeId == newCloudItemProvider.ItemTypeId);
            if (cipType is null)
            {
                throw new AppException($"Provider type: {newCloudItemProviderDto.ItemTypeId} does not exist !");
            }
            var secretsWithValues = await _secretService.GetSecretsWithValueById(newCloudItemProviderDto.SecretIds.ToArray(), newCloudItemProvider.OrganizationId);
            var cipTypeRequiredPurposes = cipType.RequiredSecretPurposes.Split(',');
            foreach (var requiredPurpose in cipTypeRequiredPurposes)
            {
                var secretWithPurpose = secretsWithValues.Find(s => s.SecretPurpose == requiredPurpose);
                if (secretWithPurpose is null)
                {
                    throw new AppException($"Secret with purpose: {requiredPurpose} not found !");
                }
            }

            secretsWithValues.ForEach(s =>
                _dataContext.CloudItemProvidersSecrets.Add(
                    new CloudItemProviderSecret
                    {
                        CloudItemProviderId = newCloudItemProvider.Id,
                        SecretId = s.SecretId
                    })
            );
            await _dataContext.SaveChangesAsync();
            return _mapper.Map<CloudItemProviderDto>(newCloudItemProvider);
        }

        public async Task<CloudItemProviderDto?> GetCloudItemProviderByNameAsync(string name)
        {
            var itemProvider = await _dataContext.CloudItemProviders.FirstOrDefaultAsync(p => p.Name == name);
            if (itemProvider == null)
            {
                return null;
            }
            return _mapper.Map<CloudItemProviderDto>(itemProvider);
        }

        public async Task<CloudItemProviderDto> GetCloudItemProviderByIdAsync(Guid Id, Guid OrganizationId)
        {
            var provider = await _dataContext.CloudItemProviders.Where(p => p.Id == Id && p.OrganizationId == OrganizationId).FirstOrDefaultAsync();
            return _mapper.Map<CloudItemProviderDto>(provider);
        }

        public async Task<List<CloudItemProviderSecretDto>> GetCloudItemProviderSecretByProviderIdAsync(Guid ProviderId)
        {
            var providerSecrets = await _dataContext.CloudItemProvidersSecrets.Where(s => s.CloudItemProviderId == ProviderId).ToListAsync();
            return _mapper.Map<List<CloudItemProviderSecretDto>>(providerSecrets);
        }
    }
}