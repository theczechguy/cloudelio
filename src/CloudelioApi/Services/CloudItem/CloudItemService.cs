﻿using AutoMapper;
using CloudelioApi.DTOs.CloudItem;
using CloudelioApi.DTOs.CloudItem.Azure;
using CloudelioApi.DTOs.Secret;
using CloudelioApi.Entities;
using CloudelioApi.Exceptions;
using CloudelioApi.Models;
using CloudelioApi.Services.AzureOrchestration;
using CloudelioApi.Services.CloudItemProvider;
using CloudelioApi.Services.CloudItemProviderType;
using CloudelioApi.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Text.Json;

namespace CloudelioApi.Services.CloudItem
{
    public class CloudItemService : ICloudItemService
    {
        private readonly DataContext _dataContext;
        private readonly IAzureOrchestratorService _azureOrchestratorService;
        private readonly ISecretService _secretService;
        private readonly ICloudItemProviderService _cloudItemProviderService;
        private readonly ICloudItemProviderTypeService _cloudItemProviderTypeService;
        private readonly IMapper _mapper;

        public CloudItemService(
            DataContext dataContext,
            IAzureOrchestratorService azureOrchestratorService,
            ISecretService secretService,
            ICloudItemProviderService cloudItemProviderService,
            ICloudItemProviderTypeService cloudItemProviderTypeService,
            IMapper mapper)
        {
            _dataContext = dataContext;
            _azureOrchestratorService = azureOrchestratorService;
            _secretService = secretService;
            _cloudItemProviderService = cloudItemProviderService;
            _cloudItemProviderTypeService = cloudItemProviderTypeService;
            _mapper = mapper;
        }

        public async Task<bool> DeleteAzureResourceGroup(DeleteCloudItemDto deleteCloudItemDto)
        {
            var cloudItemObject = await _getCloudItemById(deleteCloudItemDto.OrganizationId, deleteCloudItemDto.CloudItemId);
            var itemProperties = cloudItemObject.DeserializeProperties();
            var resourceGroupAzureId = itemProperties.First(ps => ps.Key == "id").Value;

            // get provider details
            var provider = await _cloudItemProviderService.GetCloudItemProviderByIdAsync(
              cloudItemObject.CloudItemProviderId,
              deleteCloudItemDto.OrganizationId);

            // build input for azure orchestrator
            var credentialObjectModel = await _createAzureCredentialsObject(
                deleteCloudItemDto.OrganizationId,
                provider.Id);

            var deleteRg = await _azureOrchestratorService.DeleteResourceGroupAsync(credentialObjectModel, resourceGroupAzureId);
            var deleteEntityFromDb = await _deleteCloudItemById(cloudItemObject.Id);
            return true;
        }

        public async Task<CloudItemDto> CreateNewAzureResourceGroup(RequestNewResourceGroupDto requestNewResourceGroupDto)
        {
            // get provider details
            var provider = await _cloudItemProviderService.GetCloudItemProviderByIdAsync(
              requestNewResourceGroupDto.CloudItemProviderId,
              requestNewResourceGroupDto.OrganizationId);

            // build input for azure orchestrator
            var credentialObjectModel = await _createAzureCredentialsObject(
                requestNewResourceGroupDto.OrganizationId,
                provider.Id);
            var resourceGroupDataModel = new AzureResourceGroupDataModel
            {
                Name = requestNewResourceGroupDto.Name,
                AzureLocation = requestNewResourceGroupDto.Location,
                SubscriptionId = provider.DeserializeProperties().First(ps => ps.Key == "SubscriptionId").Value
            };

            // call orchestrator
            var newResourceGroup = await _azureOrchestratorService.CreateResourceGroupAsync(credentialObjectModel, resourceGroupDataModel);

            // put it in DB
            var newCloudItem = await _storeNewCloudItem(new RegisterNewCloudItemDto
            {
                CloudGroupId = requestNewResourceGroupDto.CloudGroupId,
                CloudItemProviderId = provider.Id,
                Name = requestNewResourceGroupDto.Name,
                OrganizationId = requestNewResourceGroupDto.OrganizationId,
                Properties = new Dictionary<string, string>
                {
                    {"location", resourceGroupDataModel.AzureLocation },
                    {"provisioningState", newResourceGroup.ProvisioningState },
                    {"id", newResourceGroup.Id }
                }
            });
            return _mapper.Map<CloudItemDto>(newCloudItem);
        }

        private async Task<List<SecretWithValueDto>> _getSecretsForProviderId(Guid OrganizationId, Guid ProviderID)
        {
            // get secrets
            var providerSecrets = await _cloudItemProviderService.GetCloudItemProviderSecretByProviderIdAsync(ProviderID
            );

            var secrets = new List<SecretWithValueDto>();
            foreach (var providerSecret in providerSecrets)
            {
                var secret = await _secretService.GetSecretById(providerSecret.SecretId, OrganizationId);
                secrets.Add(secret);
            }
            return await _validateSecretsWithPurposes(secrets, ProviderID, OrganizationId);
        }

        private async Task<DB.Entities.CloudItem> _storeNewCloudItem(RegisterNewCloudItemDto registerNewCloudItemDto)
        {
            var newCloudItem = new DB.Entities.CloudItem
            {
                Name = registerNewCloudItemDto.Name,
                CloudGroupId = registerNewCloudItemDto.CloudGroupId,
                OrganizationId = registerNewCloudItemDto.OrganizationId,
                CloudItemProviderId = registerNewCloudItemDto.CloudItemProviderId,
                Properties = JsonSerializer.Serialize(registerNewCloudItemDto.Properties)
            };
            _dataContext.CloudItems.Add(newCloudItem);
            await _dataContext.SaveChangesAsync();
            return newCloudItem;
        }

        private async Task<List<SecretWithValueDto>?> _validateSecretsWithPurposes(List<SecretWithValueDto> secretWithValueDto, Guid ProviderId, Guid OrganizationId)
        {
            var provider = await _cloudItemProviderService.GetCloudItemProviderByIdAsync(ProviderId, OrganizationId);
            var providerType = await _cloudItemProviderTypeService.GetItemProviderTypeById(provider.ItemTypeId);
            var secretPurposes = providerType.RequiredSecretPurposes.Split(',');
            List<SecretWithValueDto?> output = new List<SecretWithValueDto?>();
            foreach (var purpose in secretPurposes)
            {
                var secretWithPurpose = secretWithValueDto.Find(s => s.SecretPurpose == purpose);
                if (secretWithPurpose is null)
                {
                    throw new AppException($"Secret with purpose: {purpose} not found !");
                }
                output.Add(secretWithPurpose);
            }
            return output;
        }

        private async Task<AzureCredentialObjectModel> _createAzureCredentialsObject(Guid OrganizationId, Guid ProviderId)
        {
            var secrets = await _getSecretsForProviderId(OrganizationId, ProviderId);
            return new AzureCredentialObjectModel
            {
                TenantId = secrets.First(s => s.SecretPurpose == "TenantId").SecretValue,
                ClientId = secrets.First(s => s.SecretPurpose == "ClientId").SecretValue,
                ClientSecret = secrets.First(s => s.SecretPurpose == "ClientSecret").SecretValue
            };
        }

        private async Task<CloudItemDto> _getCloudItemById(Guid organizationId, Guid CloudItemId)
        {
            var cloudItemObject = await _dataContext.CloudItems.AsNoTracking().FirstOrDefaultAsync(i => i.Id == CloudItemId && i.OrganizationId == organizationId);
            return _mapper.Map<CloudItemDto>(cloudItemObject);
        }

        private async Task<object> _deleteCloudItemById(Guid CloudItemId)
        {
            _dataContext.CloudItems.Remove(new DB.Entities.CloudItem() { Id = CloudItemId });
            return await _dataContext.SaveChangesAsync();
        }
    }
}