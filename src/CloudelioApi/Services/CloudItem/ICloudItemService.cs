﻿using CloudelioApi.DTOs.CloudItem;
using CloudelioApi.DTOs.CloudItem.Azure;

namespace CloudelioApi.Services.CloudItem
{
    public interface ICloudItemService
    {
        Task<CloudItemDto> CreateNewAzureResourceGroup(RequestNewResourceGroupDto requestNewResourceGroupDto);
        Task<bool> DeleteAzureResourceGroup(DeleteCloudItemDto deleteCloudItemDto);
    }
}