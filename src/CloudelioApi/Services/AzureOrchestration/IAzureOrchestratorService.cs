﻿using CloudelioApi.DTOs.CloudItem.Azure;
using CloudelioApi.Models;

namespace CloudelioApi.Services.AzureOrchestration
{
    public interface IAzureOrchestratorService
    {
        Task<ResourceGroupDto> CreateResourceGroupAsync(AzureCredentialObjectModel azureCredentialObjectModel, AzureResourceGroupDataModel azureResourceGroupDataModel);
        Task<object> DeleteResourceGroupAsync(AzureCredentialObjectModel azureCredentialObjectModel, string ResourceGroupId);
    }
}