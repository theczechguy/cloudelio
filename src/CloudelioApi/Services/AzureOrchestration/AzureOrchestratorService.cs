﻿using Azure.Identity;
using Azure.ResourceManager;
using Azure.ResourceManager.Resources;
using CloudelioApi.DTOs.CloudItem.Azure;
using CloudelioApi.Models;

namespace CloudelioApi.Services.AzureOrchestration
{
    public class AzureOrchestratorService : IAzureOrchestratorService
    {
        private readonly ILogger<AzureOrchestratorService> _logger;

        public AzureOrchestratorService(ILogger<AzureOrchestratorService> logger)
        {
            _logger = logger;
        }

        public async Task<ResourceGroupDto> CreateResourceGroupAsync(
            AzureCredentialObjectModel azureCredentialObjectModel,
            AzureResourceGroupDataModel azureResourceGroupDataModel)
        {
            _logger.LogInformation("Creating new resource group : {@rg}", azureResourceGroupDataModel);
            ArmClient armClient = _CreateArmClient(
                azureCredentialObjectModel,
                azureResourceGroupDataModel.SubscriptionId);

            SubscriptionResource subscription = await armClient.GetDefaultSubscriptionAsync();
            _logger.LogInformation("Subscription retrieved: {sub}", subscription.Data.DisplayName);

            ResourceGroupData rgData = new ResourceGroupData(
                azureResourceGroupDataModel.AzureLocation);
            ResourceGroupCollection resourceGroups = subscription.GetResourceGroups();
            _logger.LogInformation("Creating resource group");
            ArmOperation<ResourceGroupResource> armOperation = await resourceGroups.CreateOrUpdateAsync(
                Azure.WaitUntil.Completed,
                azureResourceGroupDataModel.Name,
                rgData);
            _logger.LogInformation("Resource group created: {@rgdata}", armOperation.Value.Data);

            return new ResourceGroupDto
            {
                Id = armOperation.Value.Data.Id,
                Location = armOperation.Value.Data.Location,
                Name = armOperation.Value.Data.Name,
                ProvisioningState = armOperation.Value.Data.ResourceGroupProvisioningState
            };
        }

        public async Task<object> DeleteResourceGroupAsync(AzureCredentialObjectModel azureCredentialObjectModel, string ResourceGroupId)
        {
            _logger.LogInformation("Deleting azure resource group: {@RG}", ResourceGroupId);
            var subId = ResourceGroupId.Split('/')[2];
            if (!Guid.TryParse(subId, out var validatedSubId))
            {
                throw new InvalidCastException("Unable to parse subscription guid");
            }
            ArmClient armClient = _CreateArmClient(
                azureCredentialObjectModel,
                validatedSubId.ToString()
            );

            SubscriptionResource subscription = await armClient.GetDefaultSubscriptionAsync();
            _logger.LogInformation("Subscription retrieved: {sub}", subscription.Data.DisplayName);

            var rgObject = await subscription.GetResourceGroupAsync(ResourceGroupId.Split('/').Last());
            _logger.LogInformation("Resource group retrieved: {@rgobject}", rgObject);
            var deleteRgOperation = await rgObject.Value.DeleteAsync(Azure.WaitUntil.Completed);
            _logger.LogInformation("Resource group deleted: {@operation}", deleteRgOperation.HasCompleted);

            return true;
        }

        private ArmClient _CreateArmClient(AzureCredentialObjectModel CredentialObject, string SubscriptionId)
        {
            ClientSecretCredential clientSecretCredential = new ClientSecretCredential(
                    CredentialObject.TenantId,
                    CredentialObject.ClientId,
                    CredentialObject.ClientSecret
                );
            return new ArmClient(clientSecretCredential, SubscriptionId);
        }
    }
}