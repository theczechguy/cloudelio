﻿using AutoMapper;
using CloudelioApi.DTOs.CloudGroup;
using CloudelioApi.DTOs.Organization;
using CloudelioApi.Entities;
using CloudelioApi.Entities.Models;
using CloudelioApi.Exceptions;
using CloudelioApi.Services.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace CloudelioApi.Services
{
    public class OrganizationService : IOrganizationService
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;
        private readonly ISecretStorageService _secretStorageService;

        public OrganizationService(DataContext dataContext, IMapper mapper, ISecretStorageService secretStorageService)
        {
            _dataContext = dataContext;
            _mapper = mapper;
            _secretStorageService = secretStorageService;
        }

        public async Task<OrganizationDto> AddOrganizationAsync(RegisterOrganizationDto registerOrganizationDto)
        {
            var organization = _mapper.Map<Organization>(registerOrganizationDto);
            var secretStorage = await _secretStorageService.PickOneSecretStorage();
            organization.SecretStorageId = secretStorage.SecretStorageId;

            _dataContext.Organizations.Add(organization);
            await _dataContext.SaveChangesAsync();

            return _mapper.Map<OrganizationDto>(organization);
        }

        public async Task DeleteOrganizationAsync(Guid organizationId)
        {
            var organization = await this.FindOrganizationByIdAsync(organizationId);

            _dataContext.Remove(organization);
            await _dataContext.SaveChangesAsync();
        }

        public async Task<OrganizationDto> FindOrganizationByIdAsync(Guid id)
        {
            var organization = await _dataContext.Organizations.FindAsync(id);
            if (organization is null)
            {
                throw new OrganizationNotFound();
            }
            return _mapper.Map<OrganizationDto>(organization);
        }

        public async Task<OrganizationDto> FindOrganizationByNameAsync(string name)
        {
            var organization = await _dataContext.Organizations.FirstOrDefaultAsync(o => o.OrganizationName == name);
            if (organization is null)
            {
                throw new OrganizationNotFound();
            }
            return _mapper.Map<OrganizationDto>(organization);
        }

        public async Task<List<CloudGroupDto>> GetCloudGroupsByOrgId(Guid guid)
        {
            var organizationDto = await FindOrganizationByIdAsync(guid);
            if (organizationDto is null)
            {
                throw new OrganizationNotFound();
            }
            var cloudGroups = await _dataContext.CloudGroups
                .Where(c => c.OrganizationId == organizationDto.OrganizationId).ToListAsync();

            return _mapper.Map<List<CloudGroupDto>>(cloudGroups);
        }
    }
}