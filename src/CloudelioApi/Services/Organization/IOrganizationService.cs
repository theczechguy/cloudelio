﻿using CloudelioApi.DTOs.CloudGroup;
using CloudelioApi.DTOs.Organization;

namespace CloudelioApi.Services
{
    public interface IOrganizationService
    {
        Task<OrganizationDto> AddOrganizationAsync(RegisterOrganizationDto registerOrganizationDto);

        Task DeleteOrganizationAsync(Guid organizationId);

        Task<OrganizationDto> FindOrganizationByIdAsync(Guid id);

        Task<OrganizationDto> FindOrganizationByNameAsync(string name);

        Task<List<CloudGroupDto>> GetCloudGroupsByOrgId(Guid guid);
    }
}