using Azure.Identity;
using CloudelioApi.Middleware;
using Microsoft.OpenApi.Models;
using Serilog;
using System.Reflection;
using static CloudelioApi.Extensions.ServiceExtensions;

Log.Logger = new LoggerConfiguration()
    .WriteTo.Console()
    .CreateBootstrapLogger();

Log.Information("=== Cloudelio API ===");
Log.Information("Application version : {version}", Assembly.GetEntryAssembly().GetName().Version);

var builder = WebApplication.CreateBuilder(args);
builder.Host.UseSerilog((ctx, lc) => lc
    .ReadFrom.Configuration(ctx.Configuration));

Log.Information("Connecting to KeyVault");
builder.Configuration.AddAzureKeyVault(
    new Uri(builder.Configuration["VaultUri"]),
    new DefaultAzureCredential(new DefaultAzureCredentialOptions
    {
        ExcludeEnvironmentCredential = true,
        ExcludeInteractiveBrowserCredential = true,
        ExcludeAzurePowerShellCredential = true,
        ExcludeSharedTokenCacheCredential = true,
        ExcludeVisualStudioCodeCredential = true,
        ExcludeVisualStudioCredential = false,
        ExcludeAzureCliCredential = true,
        ExcludeManagedIdentityCredential = false
    }));

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    options.SwaggerDoc("v1", new OpenApiInfo
    {
        Title = "Cloudelio API",
        Description = ".NET Core Web API for Cloudelio"
    });
});

builder.Services.ConfigureSqlServerContext(builder.Configuration);
builder.Services.ConfigureAutoMapper();
builder.Services.ConfigureAppServices();

var app = builder.Build();

// Configure the HTTP request pipeline.

app.UseSwagger();
app.UseSwaggerUI(options =>
{
    options.SwaggerEndpoint("/swagger/v1/swagger.json", "v1");
    options.RoutePrefix = string.Empty;
});

app.UseAuthorization();

app.MapControllers();
app.UseSerilogRequestLogging();

app.UseMiddleware<ExceptionMiddleware>();

app.Run();