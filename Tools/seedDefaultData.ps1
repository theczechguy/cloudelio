param(
    $ApiUri = 'http://localhost:5109'
)

#region setup
    $contentType = 'application/json'
#endregion

#region add secret storage
    $kvUri = 'https://cloudelio-testvault.vault.azure.net/'
    $body = @{
        vaultUri = $kvUri
    } | ConvertTo-Json
    Invoke-RestMethod -Method Post -Uri "$ApiUri/api/SecretStorages" -ContentType $contentType -Body $body
#endregion

#region organization
    $organization = Invoke-RestMethod -Method Post -Uri "$ApiUri/api/organizations" -ContentType $contentType -Body (@{
        'organizationName' = 'Dummy Org'
        'organizationOwner' = 'm@m.z'
    } | convertto-json)
#endregion

#region secrests for CI provider
    $secrets = @(
        @{
            SecretName = 'azure-rg-client-secret'
            SecretValue = 'i0n8Q~MlwUTtjtdH16l3CxD-kL37y3OKYzBiTb_m'
            OrganizationId = $organization.organizationid
        },
        @{
            SecretName = 'azure-rg-client-id'
            SecretValue = '01dff5e7-1991-41f3-9647-92029859ae32'
            OrganizationId = $organization.organizationid
        },
        @{
            SecretName = 'azure-rg-tenant-id'
            SecretValue = 'a11b05df-f04b-4563-8f3f-c84f80060282'
            OrganizationId = $organization.organizationid
        }
        ) | foreach -Process {
            $params = @{
                Method = 'Post'
                ContentTYpe = $contentType
                Uri = "$ApiUri/api/organizations/$($organization.organizationid)/secrets"
                Body = $_ | ConvertTo-Json
            }
            Invoke-RestMethod @params
        }
#endregion

#region register CI provider
 $body = @{
    'name' = 'Test sub RG'
    'description' = 'resource group in test sub'
    'itemTypeId' = '83e33424-c8f2-4400-bc2f-b2a177191278'
    'organizationId' = '98d01341-12bb-40d3-d464-08dacb26acce'
    'properties' = @{
        'subscriptionId' = '91a667f9-8c8e-4a12-b4dc-9da0f2e63e93'
        'azureRegion' = 'euwest'
    }
    'secretIds' = $secrets.secretid
 }
 $params = @{
    Method = 'Post'
    ContentType = $contentType
    Uri = "$ApiUri/api/clouditemprovider/"
    Body = $body | ConvertTo-Json
 }
 $ciProvider = Invoke-RestMethod @params
#endregion